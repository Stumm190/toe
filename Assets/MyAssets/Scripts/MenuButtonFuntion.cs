﻿using UnityEngine;
using System.Collections;

public class MenuButtonFuntion : MonoBehaviour {

    [SerializeField]
    private GameObject[] menuPanel;

    public void activateMenuButton(int index)
    {
        foreach (GameObject g in menuPanel)
        {
            g.SetActive(false);
        }

        menuPanel[index].SetActive(true);
    }
}
