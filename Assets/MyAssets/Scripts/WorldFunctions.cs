﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class WorldFunctions : MonoBehaviour {

    //just don't ask....
    public static void LoadScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }
    //I know there is already a static method for this...
    public void loadScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }
    public void debugFunc()
    {
        Debug.Log("Look at me! I'm working!");
    }
    public void restart()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }
    public void exit()
    {
        Application.Quit();
    }
}
