﻿using UnityEngine;
using System.Collections;

public class Heli : MonoBehaviour, BasicObject {

    [SerializeField]
    private float x;

    [SerializeField]
    private float y;

    [SerializeField]
    private float z;

    [Range(0, 1000)]
    public float basicSpeed = 0f;
    private float speed;

    [SerializeField]
    private GameObject mainRotor;

    [SerializeField]
    private GameObject backRotor;

    public void triggerButton(int config, float triggerAxis, ControllerScriptNew source)
    {
        //transform.rotation = Quaternion.Lerp(transform.rotation, source.transform.rotation, 0.01f * triggerAxis);
        speed = Mathf.Lerp(speed, triggerAxis * 2000f, 0.1f);
        Debug.Log(source.transform.rotation);
        GetComponent<Rigidbody>().AddForce(transform.up * triggerAxis * 30f);
    }
    public void gripButton(int config, ControllerScriptNew source)
    {
        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                if (source.getAttachedObject() == this.gameObject)
                {
                    basicSpeed = 0f;
                    GameObject.Find("[CameraRig]").transform.SetParent(null);
                    GameObject.Find("[CameraRig]").transform.rotation = Quaternion.identity;
                    source.otherController.GetComponent<ControllerScriptNew>().setBasicObject(this);
                    source.otherController.GetComponent<ControllerScriptNew>().setAttachedObject(null);
                    source.setAttachedObject(null);
                    source.setBasicObject(null);
                    GetComponent<HeliFlight>().enabled = false;
                }
                break;
            default:
                break;
        }

    }
    public void menuButton(int config, ControllerScriptNew source)
    {

    }

    public void touchpadButton(int config, Vector2 touchpadAxis, ControllerScriptNew source)
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, source.transform.rotation * Quaternion.Euler(x, y, z), 0.01f);
    }

    public void initObject(ControllerScriptNew source)
    {
        GameObject.Find("[CameraRig]").transform.SetParent(transform);
        GameObject.Find("[CameraRig]").transform.position = transform.position;
        GameObject.Find("[CameraRig]").transform.rotation = transform.rotation;
        GetComponent<HeliFlight>().initFlight(source, source.otherController.GetComponent<ControllerScriptNew>());
        if (source.otherController.GetComponent<ControllerScriptNew>().getBasicObject() != null)
        {
            source.otherController.GetComponent<ControllerScriptNew>().getBasicObject().drop();
        }
        source.otherController.GetComponent<ControllerScriptNew>().setAttachedObject(this.gameObject);
        source.otherController.GetComponent<ControllerScriptNew>().setBasicObject(this);
        basicSpeed = 500f;
    }

    public void drop()
    {
        
    }

    public Sprite getThumbnail()
    {
        return null;
    }
    
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        mainRotor.transform.Rotate(Vector3.up * Time.deltaTime * Mathf.Lerp(speed, speed + basicSpeed, 0.5f));
        backRotor.transform.Rotate(Vector3.up * Time.deltaTime * Mathf.Lerp(speed, speed + basicSpeed, 0.5f));
	}
}
