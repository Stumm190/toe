﻿using UnityEngine;
using System.Collections;

public class HeliRotationAnimation : MonoBehaviour {

    [Range(0, 10000)]
    public float speed;

    [SerializeField]
    private GameObject mainRotor;

    [SerializeField]
    private GameObject backRotor;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        mainRotor.transform.Rotate(Vector3.up * Time.deltaTime * speed);
        backRotor.transform.Rotate(Vector3.up * Time.deltaTime * speed);
	}
}
