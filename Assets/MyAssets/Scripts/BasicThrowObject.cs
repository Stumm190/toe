﻿using UnityEngine;
using System.Collections;

public class BasicThrowObject : MonoBehaviour, BasicObject 
{
    public Sprite thumbnail;

    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
   
	}

    public void triggerButton(int config, float triggerAxis, ControllerScriptNew source) {
      //  Debug.Log(triggerAxis * 255);
       // GetComponent<Renderer>().material.color = new Color(triggerAxis * 255, 0, 0);
    }
    public void gripButton(int config, ControllerScriptNew source) 
    {
        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                transform.SetParent(source.gameObject.transform);
                rb.isKinematic = true;
                break;
            case 2: //down
                transform.SetParent(source.gameObject.transform);
                rb.isKinematic = true;
                break;

            case 4: //up
                transform.SetParent(null);
                source.setAttachedObject(null);
                source.setBasicObject(null);
                rb.isKinematic = false;
                 Vector3 median = new Vector3();
                        foreach (Vector3 v in source.getControllerPosStorage())
                        {
                            median += v;
                        }
                        median /= source.CPSSize;
                        Vector3 force = (median - source.getControllerPosStorage()[source.counter]) * 4000;
                        rb.AddForce(force);
                break;
            default:
                break;
        }
    }
    public void menuButton(int config, ControllerScriptNew source) { }

    public void touchpadButton(int config, Vector2 touchpadAxis, ControllerScriptNew source) { }
    public void initObject(ControllerScriptNew source)
    {

    }
    public void drop()
    {
        if(transform.parent != null) {
            transform.SetParent(null);
            rb.isKinematic = false;
        }
    }
    public Sprite getThumbnail()
    {
        return thumbnail;
    }
}
