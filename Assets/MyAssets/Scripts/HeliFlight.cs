﻿using UnityEngine;
using System.Collections;

public class HeliFlight : MonoBehaviour {
    public GameObject firstController;
    public GameObject secondController;
    private ControllerScriptNew firstControllerScript;
    private ControllerScriptNew secondControllerScript;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void initFlight(ControllerScriptNew first, ControllerScriptNew second)
    {
        enabled = true;
        firstControllerScript = first;
        secondControllerScript = second;
    }
}
