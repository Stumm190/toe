﻿using UnityEngine;
using System.Collections;
/*Class to drive the Kart*/
public class Driving : MonoBehaviour {
    public GameObject firstController;
    public GameObject secondController;
    private ControllerScriptNew firstControllerScript;
    private ControllerScriptNew secondControllerScript;
    [SerializeField]
    private WheelCollider leftFrontWheel;

    [SerializeField]
    private WheelCollider rightFrontWheel;

    [SerializeField]
    private WheelCollider leftBackWheel;

    [SerializeField]
    private WheelCollider rightBackWheel;

    [SerializeField]
    private float maxTorque = 260.0f;   //Drehmoment bzw. Schnelligkeit

	
	// Update is called once per frame
	void FixedUpdate () {
        //acceleration of the Kart
        leftFrontWheel.motorTorque = maxTorque * (((firstControllerScript.getTriggerAxis() > 0.05f) ? firstControllerScript.getTriggerAxis() : 0) + ((secondControllerScript.getTriggerAxis() > 0.05f) ? -secondControllerScript.getTriggerAxis() : 0));
        rightFrontWheel.motorTorque = maxTorque * (((firstControllerScript.getTriggerAxis() > 0.05f) ? firstControllerScript.getTriggerAxis() : 0) + ((secondControllerScript.getTriggerAxis() > 0.05f) ? -secondControllerScript.getTriggerAxis() : 0));
        leftBackWheel.motorTorque = maxTorque * (((firstControllerScript.getTriggerAxis() > 0.05f) ? firstControllerScript.getTriggerAxis() : 0) + ((secondControllerScript.getTriggerAxis() > 0.05f) ? -secondControllerScript.getTriggerAxis() : 0));
        rightBackWheel.motorTorque = maxTorque * (((firstControllerScript.getTriggerAxis() > 0.05f) ? firstControllerScript.getTriggerAxis() : 0) + ((secondControllerScript.getTriggerAxis() > 0.05f) ? -secondControllerScript.getTriggerAxis() : 0));

        //Steering of the Kart
        leftFrontWheel.steerAngle = -50 * firstControllerScript.steigung;
        rightFrontWheel.steerAngle = -50 * firstControllerScript.steigung;
    }
    public void initDriving(ControllerScriptNew first, ControllerScriptNew second)
    {
        enabled = true;
        firstControllerScript = first;
        secondControllerScript = second;
    }
}
