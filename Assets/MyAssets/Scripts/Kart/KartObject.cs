﻿using UnityEngine;
using System.Collections;

public class KartObject : MonoBehaviour, BasicObject {

    public Sprite thumbnail;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void triggerButton(int config, float triggerAxis, ControllerScriptNew source) { }
    public void gripButton(int config, ControllerScriptNew source)
    {
        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                if (source.getAttachedObject() == this.gameObject)
                {
                    GameObject.Find("[CameraRig]").transform.SetParent(null);
                    GameObject.Find("[CameraRig]").transform.rotation = Quaternion.identity;
                    source.otherController.GetComponent<ControllerScriptNew>().setBasicObject(null);
                    source.otherController.GetComponent<ControllerScriptNew>().setAttachedObject(null);
                    source.setAttachedObject(null);
                    source.setBasicObject(null);
                    GetComponent<Driving>().enabled = false;
                }
                break;
            default:
                break;
        }
       
    }
    
    public void menuButton(int config, ControllerScriptNew source) { }

    public void touchpadButton(int config, Vector2 touchpadAxis, ControllerScriptNew source) { }

    public void initObject(ControllerScriptNew source)
    {
        GameObject.Find("[CameraRig]").transform.SetParent(transform);
        GameObject.Find("[CameraRig]").transform.position = transform.position;
        GameObject.Find("[CameraRig]").transform.rotation = transform.rotation;
        GetComponent<Driving>().initDriving(source, source.otherController.GetComponent<ControllerScriptNew>());
        if (source.otherController.GetComponent<ControllerScriptNew>().getBasicObject() != null)
        {
            source.otherController.GetComponent<ControllerScriptNew>().getBasicObject().drop();
        }
        source.otherController.GetComponent<ControllerScriptNew>().setAttachedObject(this.gameObject);
        source.otherController.GetComponent<ControllerScriptNew>().setBasicObject(this);
    }
    public void drop()
    {
      
    }
    public Sprite getThumbnail()
    {
        return thumbnail;
    }
}
