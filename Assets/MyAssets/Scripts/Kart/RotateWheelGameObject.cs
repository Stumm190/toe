﻿using UnityEngine;
using System.Collections;

public class RotateWheelGameObject : MonoBehaviour {

    [SerializeField]
    private GameObject frontLeftWheel;

    [SerializeField]
    private GameObject frontLeftWheelNavigation;

    [SerializeField]
    private WheelCollider frontLeftWheelCollider;

    [SerializeField]
    private GameObject frontRightWheel;

    [SerializeField]
    private GameObject frontRightWheelNavigation;

    [SerializeField]
    private WheelCollider frontRightWheelCollider;

    [SerializeField]
    private GameObject backLeftWheel;

    [SerializeField]
    private WheelCollider backLeftWheelCollider;

    [SerializeField]
    private GameObject backRightWheel;

    [SerializeField]
    private WheelCollider backRightWheelCollider;

    [SerializeField]
    private GameObject lenkRad;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        frontLeftWheelNavigation.transform.localEulerAngles = new Vector3(frontLeftWheelNavigation.transform.localEulerAngles.x, 
            frontLeftWheelCollider.steerAngle - frontLeftWheelNavigation.transform.localEulerAngles.z,
            frontLeftWheelNavigation.transform.localEulerAngles.z);

        frontRightWheelNavigation.transform.localEulerAngles = new Vector3(frontRightWheelNavigation.transform.localEulerAngles.x, 
            frontRightWheelCollider.steerAngle - frontRightWheelNavigation.transform.localEulerAngles.z,
            frontRightWheelNavigation.transform.localEulerAngles.z);
        
        lenkRad.transform.localEulerAngles = new Vector3(lenkRad.transform.localEulerAngles.x, lenkRad.transform.localEulerAngles.y, frontRightWheelNavigation.transform.localEulerAngles.y - 90);

        frontLeftWheel.transform.Rotate(0, 0, frontLeftWheelCollider.rpm / 60 * 360 * Time.deltaTime);
        frontRightWheel.transform.Rotate(0, 0, -frontRightWheelCollider.rpm / 60 * 360 * Time.deltaTime);
        backLeftWheel.transform.Rotate(0, 0, -backLeftWheelCollider.rpm / 60 * 360 * Time.deltaTime);
        backRightWheel.transform.Rotate(0, 0, -backRightWheelCollider.rpm / 60 * 360 * Time.deltaTime);
    }
}
