﻿using UnityEngine;
using System.Collections;

public class MovingScript : MonoBehaviour {

    private GameObject target;

    // Use this for initialization
    void Start () {
        target = this.gameObject;
	}

    void FixedUpdate()
    {
        Vector2 pos = Vector2.Lerp((Vector2)transform.position, (Vector2)target.transform.position, Time.fixedDeltaTime*5f);
        transform.position = new Vector3(pos.x, pos.y, target.transform.position.z);
    }

    public void setNewPosition(GameObject target)
    {
        this.target = target;
    }
}
