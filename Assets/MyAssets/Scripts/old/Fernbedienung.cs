﻿using UnityEngine;
using System.Collections;

public class Fernbedienung : MonoBehaviour, BasicObject {

    public Sprite thumbnail;
    private Rigidbody rb;
    public Transform direction;
    public float laserThickness = 0.002f;
    private GameObject pointer;
   
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pointer.transform.SetParent(transform, false);
        pointer.transform.localScale = new Vector3(laserThickness, laserThickness, 100.0f);
        pointer.transform.localPosition = new Vector3(0.0f, 0.0f, 50.0f);
        Object.DestroyImmediate(pointer.GetComponent<BoxCollider>());

        Material newMaterial = new Material(Shader.Find("Wacki/LaserPointer"));

        newMaterial.SetColor("_Color", Color.red);
        pointer.GetComponent<MeshRenderer>().material = newMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hitInfo;
        bool bHit = Physics.Raycast(ray, out hitInfo);

        float distance = 100.0f;

        if (bHit)
        {
            distance = hitInfo.distance;
        }

        pointer.transform.localScale = new Vector3(laserThickness, distance, laserThickness);
        pointer.transform.localPosition = new Vector3(0.0f,-distance * 0.5f, 0.0f);

            pointer.SetActive(true);


    }
    

    public void triggerButton(int config, float triggerAxis, ControllerScriptNew source)
    {
        switch (config)
        {
            case 1: //pressed

                break;
            case 3: //Down&&pressed
                
                break;
            default:
                break;
        }


    }
    public void gripButton(int config, ControllerScriptNew source)
    {
        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                if (transform.parent == source.gameObject.transform)
                {
                    transform.SetParent(null);
                    source.setAttachedObject(null);
                    source.setBasicObject(null);
                    rb.isKinematic = false;
                }
                break;
            case 2: //down

                transform.SetParent(source.gameObject.transform);
                transform.position = source.gameObject.transform.position;
                transform.rotation = source.gameObject.transform.rotation;
                rb.isKinematic = true;
                break;

            default:
                break;
        }
    }
    public void menuButton(int config, ControllerScriptNew source) { }

    public void touchpadButton(int config, Vector2 touchpadAxis, ControllerScriptNew source) { }

    public void initObject(ControllerScriptNew source)
    {

    }
    public void drop()
    {
        if (transform.parent != null)
        {
            transform.SetParent(null);
            rb.isKinematic = false;
        }
    }
    
    public Sprite getThumbnail()
    {
        return thumbnail;
    }
}
