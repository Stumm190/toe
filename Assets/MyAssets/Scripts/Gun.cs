﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour, BasicObject
{
    public Sprite thumbnail;
    private Rigidbody rb;
    public Transform direction;
    public GameObject bullet;
    [Range(100,10000)] public float bulletSpeed;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void triggerButton(int config, float triggerAxis, ControllerScriptNew source) 
    {
        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                GameObject g = (GameObject)GameObject.Instantiate(bullet, direction.position, direction.rotation);
                g.GetComponent<Rigidbody>().AddForce((direction.position - transform.position).normalized * bulletSpeed);
                break;
            default:
                break;
        }

        
    }
    public void gripButton(int config, ControllerScriptNew source)
    {
        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                if (transform.parent == source.gameObject.transform)
                {
                    transform.SetParent(null);
                    source.setAttachedObject(null);
                    source.setBasicObject(null);
                    rb.isKinematic = false;
                }
                break;
            case 2: //down

                transform.SetParent(source.gameObject.transform);
                transform.position = source.gameObject.transform.position;
                transform.rotation = source.gameObject.transform.rotation;
                rb.isKinematic = true;
                break;

            default:
                break;
        }
    }
    public void menuButton(int config, ControllerScriptNew source) { }

    public void touchpadButton(int config, Vector2 touchpadAxis, ControllerScriptNew source) { }

    public void initObject(ControllerScriptNew source)
    {

    }
    public void drop()
    {
        if(transform.parent != null) {
            transform.SetParent(null);
            rb.isKinematic = false;
        }
    }
    public Sprite getThumbnail()
    {
        return thumbnail;
    }
}
