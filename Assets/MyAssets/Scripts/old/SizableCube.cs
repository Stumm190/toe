﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SizableCube : MonoBehaviour {

    private Mesh mesh;
    Vector3[] vertices;

    void Awake()
    {
        	GetComponent<MeshFilter>().mesh = mesh = new Mesh();
		mesh.name = "Procedural Grid";
	    vertices = new Vector3[8];
        for (int x = 0; x <= 1; x++)
        {
            for (int y = 0; y <= 1; y++)
            {
                for (int z = 0; z <= 1; z++)
                {
                    vertices[z + 2 * y + 4 * x] = new Vector3(x, y, z);
                }
            }
        }
        int[] triangles = new int[36];
        mesh.vertices = vertices;
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;

        triangles[3] = 1;
        triangles[4] = 3;
        triangles[5] = 2;
        
        triangles[6] = 1;
        triangles[7] = 7;
        triangles[8] = 3;

        triangles[9] = 1;
        triangles[10] = 5;
        triangles[11] = 7;

        triangles[12] = 5;
        triangles[13] = 6;
        triangles[14] = 7;

        triangles[15] = 5;
        triangles[16] = 4;
        triangles[17] = 6;

        triangles[18] = 0;
        triangles[19] = 6;
        triangles[20] = 4;

        triangles[21] = 0;
        triangles[22] = 2;
        triangles[23] = 6;

        triangles[24] = 2;
        triangles[25] = 3;
        triangles[26] = 7;

        triangles[27] = 2;
        triangles[28] = 7;
        triangles[29] = 6;

        triangles[30] = 0;
        triangles[31] = 5;
        triangles[32] = 1;

        triangles[33] = 0;
        triangles[34] = 4;
        triangles[35] = 5;

        mesh.triangles = triangles;
    }
	// Use this for initialization
	void Start () {
        
	}
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(vertices[i], 0.1f);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
