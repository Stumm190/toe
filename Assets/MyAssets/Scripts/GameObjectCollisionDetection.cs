﻿using UnityEngine;
using System.Collections;

public class GameObjectCollisionDetection : MonoBehaviour {

    [SerializeField]
    private GameObject colliderVase;

    [SerializeField]
    private GameObject[] vaseFragments;

    private bool isCollision;

    void Start()
    {
        isCollision = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if(!isCollision)
        {
            if(collision.gameObject.tag != "Terrain")
            {
                if(collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude > 15f || GetComponent<Rigidbody>().velocity.magnitude > 15f)
                {
                    isCollision = true;
                    destroyVase();
                }
            } else
            {
                if (GetComponent<Rigidbody>().velocity.magnitude > 1f)
                {
                    isCollision = true;
                    destroyVase();
                }
            }
        }
    }

    private void destroyVase()
    {
        Destroy(GetComponent<Rigidbody>());
        Destroy(colliderVase);

        for (int counter = 0; counter < vaseFragments.Length; counter++)
        {
            vaseFragments[counter].AddComponent<Rigidbody>();
            vaseFragments[counter].GetComponent<Rigidbody>().mass = 10;
            vaseFragments[counter].GetComponent<Collider>().enabled = true;
        }

        StartCoroutine(destroyFragments());
    }

    IEnumerator destroyFragments()
    {
        yield return new WaitForSeconds(10);
        Destroy(this.gameObject);
    }
}
