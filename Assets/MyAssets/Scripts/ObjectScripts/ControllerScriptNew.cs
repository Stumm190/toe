﻿using UnityEngine;
using System.Collections;
using Wacki;

/*This Script is attached to a Controller. It manages most of the Controllerfunctions*/
public class ControllerScriptNew : MonoBehaviour {
    private Valve.VR.EVRButtonId gripBtn = Valve.VR.EVRButtonId.k_EButton_Grip;
    private Valve.VR.EVRButtonId triggerBtn = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;

    //Size of the controller positon storage 
    public int CPSSize;
    //the controller positon storage saves the last transforms of the Controller to log the Movement of the Controller
    private Vector3[] controllerPosStorage;
    //counter for the CPSStorage
    public int counter = 0;

    private int grip;
    private int trigger;
    private int touchpad;
    private int menuButton;
    
    //variable to check if the collider of the controller is triggered 
    private bool isTriggered = false;

    private bool triggerBtnLock = true;

    //reference on the Head mount Display
    private GameObject HMDeye;
    [SerializeField]
    //attachedObject is the object that is attached to the controller
    private GameObject attachedObject;
    private BasicObject basicObject;

    //triggerObject is the object that actually stays in the controller collider
    [SerializeField]
    private GameObject triggerObject;

    public Vector3 neigungsVektor;
    public float steigung;
    public GameObject spawnPos = null;
    public GameObject portPointObject;
    private Vector3 velo = new Vector3();
    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;
    private Vector3 holoRight = Vector3.right;
    private Vector3 holoForward = Vector3.forward;

    public GameObject spawnHolo;
    public GameObject controllerMenu;
    public GameObject spawnObject;
    public GameObject otherController;
    private ControllerScriptNew otherControllerScript;

    public int getTrigger()
    {
        return trigger;
    }
    //get the axis of the triggerbutton
    public float getTriggerAxis()
    {
        return controller.GetAxis(triggerBtn).x;
    }
    public void setAttachedObject(GameObject attachedObject)
    {
        this.attachedObject = attachedObject;
    }
    public GameObject getAttachedObject()
    {
        return this.attachedObject;
    }
    public BasicObject getBasicObject()
    {
        return this.basicObject;
    }
    public void setBasicObject(BasicObject basicObject)
    {
        this.basicObject = basicObject;
    }
    public Vector3[] getControllerPosStorage() { return controllerPosStorage; }
    

    void Start()
    {
        controllerPosStorage = new Vector3[CPSSize];
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        otherControllerScript = otherController.GetComponent<ControllerScriptNew>();
        HMDeye = GameObject.Find("Camera (eye)");
        if (basicObject == null && attachedObject != null)
        {
            basicObject = attachedObject.GetComponent<BasicObject>();
        }
    }
	
	// Update is called once per frame
	void Update () {
        counter = (counter + 1) % CPSSize;
        grip = 0;
        trigger = 0;
        touchpad = 0;
        menuButton = 0;
        trigger += controller.GetPressDown(triggerBtn) ? 1 : 0;
        trigger += controller.GetPress(triggerBtn) ? 2 : 0;
        trigger += controller.GetPressUp(triggerBtn) ? 4 : 0;
        touchpad += controller.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad) ? 1 : 0;
        touchpad += controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad) ? 2 : 0;
        touchpad += controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) ? 4 : 0;
        grip += controller.GetPressDown(gripBtn) ? 1 : 0;
        grip += controller.GetPress(gripBtn) ? 2 : 0;
        grip += controller.GetPressUp(gripBtn) ? 4 : 0;
        menuButton += controller.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu) ? 1 : 0;
        menuButton += controller.GetPress(SteamVR_Controller.ButtonMask.ApplicationMenu) ? 2 : 0;
        menuButton += controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu) ? 4 : 0;

        neigungsVektor = (transform.localPosition - otherController.transform.localPosition).normalized;
        steigung = neigungsVektor.y / neigungsVektor.x;
        if (steigung > 1)
            steigung = 1;
        if (steigung < -1)
            steigung = -1;

        if (attachedObject == null)
        {
            if (touchpad > 0)
                teleport(touchpad);

         
            if (controller.GetAxis(triggerBtn).x == 1 && triggerBtnLock)
            {
                instatiateGameObject(3);
                triggerBtnLock = false;

            }
                

            if (controller.GetAxis(triggerBtn).x < 1)
            {
                triggerBtnLock = true;
            }

            if (!otherController.GetComponent<ControllerScriptNew>().controllerMenu.activeSelf && controller.GetAxis(triggerBtn).x > 0.1 && controller.GetAxis(triggerBtn).x < 1)
            {
                spawnHolo.SetActive(true);
                spawnHolo.transform.position = spawnPos.transform.position;
                if (controller.GetTouch(SteamVR_Controller.ButtonMask.Touchpad))
                {
                    spawnHolo.transform.Rotate(spawnHolo.transform.parent.right * Time.deltaTime * controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad).y * -200, Space.World);

                    spawnHolo.transform.Rotate(spawnHolo.transform.parent.forward * Time.deltaTime * controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad).x * -200,Space.World);
                    Debug.Log(controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad));
                }

            }
            if (controller.GetAxis(triggerBtn).x < 0.1)
                spawnHolo.SetActive(false);

            if (menuButton > 0)
                menuButtonFunction(menuButton);

            if (grip > 0)
                gripButtonFunction(grip);
            
            
        }
        else
        {
            if (controller.GetAxis(triggerBtn).x > 0.01f)
                basicObject.triggerButton(trigger, controller.GetAxis(triggerBtn).x, this);

            if (controller.GetTouch(SteamVR_Controller.ButtonMask.Touchpad))
                basicObject.touchpadButton(touchpad, controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad), this);

            if (grip > 0)
            {
                basicObject.initObject(this);
                basicObject.gripButton(grip, this);
            }
        
            if (menuButton > 0)
                basicObject.menuButton(menuButton, this);
            
        }

        controllerPosStorage[counter] = transform.position;
        if (controllerMenu.transform.parent == transform)
        {
            controllerMenu.transform.localPosition = new Vector3((getMenuSide() ? -0.4f : 0.3f), controllerMenu.transform.localPosition.y, controllerMenu.transform.localPosition.z);
        }
	}

    //this function teleports the player
    private void teleport(int config)
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, spawnPos.transform.position - transform.position);
        if (Physics.Raycast(ray, out hit, 25f))
        {
            switch (config)
            {
                case 1: //pressed
                case 3: //Down&&pressed
                    portPointObject.SetActive(true);
                    break;
                case 2: //down
                    portPointObject.transform.position = hit.point;
                    break;

                case 4: //up
                    transform.parent.transform.position = hit.point;
                    portPointObject.SetActive(false);
                    break;
                default:
                    break;
            }
        }
        else
        {
            Ray ray2 = new Ray(transform.position + 50f * (spawnPos.transform.position - transform.position), new Vector3(0, -1, 0));
            
            if (Physics.Raycast(ray2, out hit, 10000f))
            {
                switch (config)
                {
                    case 1: //pressed
                    case 3: //Down&&pressed
                        portPointObject.SetActive(true);
                        break;
                    case 2: //down
                        portPointObject.transform.position = hit.point;
                        break;

                    case 4: //up
                        transform.parent.transform.position = hit.point;
                        portPointObject.SetActive(false);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    //Spawns the Spawnobject
    private void instatiateGameObject(int config)
    {
  
        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                if (!otherController.GetComponent<ControllerScriptNew>().controllerMenu.activeSelf)
                {
                    if (spawnObject != null)
                    Instantiate(spawnObject, spawnPos.transform.position, spawnHolo.transform.rotation);
                }
                break;
            default:
                break;
        }

    }

    //Opens the ControllerMenu
    private void menuButtonFunction(int config)
    {

        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                if (controllerMenu != null)
                {

                    if (controllerMenu.activeSelf)
                    {
                        
                        controllerMenu.SetActive(false);
                        gameObject.GetComponent<ViveUILaserPointer>().isOn = false;
                       otherController.GetComponent<ViveUILaserPointer>().isOn = false;

                    }
                    else
                    {
                        
                        otherController.GetComponent<ViveUILaserPointer>().isOn = true;
                   
                        controllerMenu.SetActive(true);
                        controllerMenu.transform.position = transform.position;
                        controllerMenu.transform.rotation = transform.rotation;
                        controllerMenu.transform.SetParent(transform);
                        controllerMenu.transform.localEulerAngles = new Vector3(90f, 0, 0);
                        if (controllerMenu.GetComponentInChildren<SpawnMenuButton>() != null)
                        controllerMenu.GetComponentInChildren<SpawnMenuButton>().setController();
                       
                    }

                }
                break;
            default:
                break;
        }

    }
    //attaches Object to the Controller
    private void gripButtonFunction(int config)
    {
        switch (config)
        {
            case 1: //pressed
            case 3: //Down&&pressed
                if(triggerObject == null){
                    attachedObject = null;
                    basicObject = null;
                }
                else
                {
                   
                    //triggerObject.transform.parent == otherController.transform
                    
                    if (triggerObject.transform.parent == otherController.transform)
                    {
                        
                        otherControllerScript .setAttachedObject(null);
                        otherControllerScript.setBasicObject(null);
                    }
                    attachedObject = triggerObject;
                    basicObject = attachedObject.GetComponent<BasicObject>();
                }
                break;
            default:
                break;
        }
    }
    //highlights the Object on Trigger Enter
    void onTriggerEnter(Collider other)
    {
        if(other.tag == "Item") {
            triggerObject = other.gameObject;
            Debug.Log("blub");
            if (other.GetComponent<Renderer>() != null)
            {
                Color tempColor = other.GetComponent<Renderer>().material.color;
                other.GetComponent<Renderer>().material.shader = Shader.Find("Custom/Highlight");
                other.GetComponent<Renderer>().material.color = tempColor;
            }
        }
    }
    //changes the shader
    void OnTriggerExit(Collider other)
    {
        if (triggerObject == other.gameObject)
        {
            triggerObject = null;
        }
        isTriggered = false;
        if (other.GetComponent<Renderer>() != null)
        {
            Color tempColor = other.GetComponent<Renderer>().material.color;
            other.GetComponent<Renderer>().material.shader = Shader.Find("MK/MKToon/Default Outline");
            other.GetComponent<Renderer>().material.color = tempColor;
        }

    }
    void OnTriggerStay(Collider other)
    {
        triggerObject = other.gameObject;
        isTriggered = true;
        if (other.GetComponent<Renderer>() != null)
        {
            Color tempColor = other.GetComponent<Renderer>().material.color;
            other.GetComponent<Renderer>().material.shader = Shader.Find("Custom/Highlight");
            other.GetComponent<Renderer>().material.color = tempColor;
        }

    }
    //sets the Spawnable Object

    public void chooseSpawnObject(GameObject go)
    {
        spawnObject = go;
        if (go.GetComponent<MeshFilter>() != null)
        {
            spawnHolo.transform.localScale = go.transform.localScale;
            spawnHolo.GetComponent<MeshFilter>().mesh = go.GetComponent<MeshFilter>().sharedMesh;
        }
        else
        {
            Debug.Log("kein Holomesh gefunden.");
        }
    }

    //returns on which side of the controller you are looking
    private bool getMenuSide() {
       
        Vector3 vektor = HMDeye.transform.GetChild(0).transform.position - HMDeye.transform.position;
        vektor.y = 0;
        Vector3 directionV = transform.position - HMDeye.transform.position;
        directionV.y = 0;
        return Vector3.Cross(vektor, directionV).y > 0;

    }

}
