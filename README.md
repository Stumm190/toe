# Sandboxumgebung für die VR-Brille HTC-VIVE #

Dieses repository enthält eine Sandboxumgebung für die HTC Vive.
Das Projekt wurde in Unity entwickelt.
Voraussetzung um an dem Projekt zu arbeiten ist das SteamVR plugin für Unity.

____

## Wie fange ich an?##
Um das System in Unity zu nutzen, müssen folgende Prefabs aus dem MyAssets/Prefab Ordner in der Scene genutzt werden:

* [CameraRig]: Das CameraRig Prefab ist das wichtigste Prefab. Es ist eine Modifizierte Version des CameraRig des SteamVR Plugin und beinhaltet die Controller und HMD Steuerung sowie den Spielbereich.
* Worldfunctions: Worldfunctions beinhaltet die Klasse Worldfunctions sowie die beiden Portpoint Objekte und den Pool mit den spawnbaren Objekten.
* MenuCanvas: Das MenuCanvas ist das Controllermenü.

Um alle Funktionen nutzen zu können, müssen folgende Elemente in den Objekten verknüpft werden:

* An den beiden Controllern unter CameraRig hängt jeweils das ControllerScriptNew

 ![1.png](https://bitbucket.org/repo/aEzoBe/images/2175526478-1.png)

![2.png](https://bitbucket.org/repo/aEzoBe/images/3952116050-2.png)
 
Bei beiden Controllern müssen nun das passende WorldFunctions/Portpoint Objekt als Port Point Object sowie das MenuCanvas als Controller Menu eingesetzt werden.
 
![3.png](https://bitbucket.org/repo/aEzoBe/images/2906775285-3.png)

In dem MenuCanvas Objekt muss unter BackgroundPanel/ObjectPanel im SpawnMenuButton Script WorldObjects/Pool als Pool Object eingesetzt werden.
 
![4.png](https://bitbucket.org/repo/aEzoBe/images/1322183897-4.png)

![5.png](https://bitbucket.org/repo/aEzoBe/images/2411171770-5.png)

___

## Das Interaktionssystem ##

Da in dem Projekt verschiedene Objekte sind, mit denen interagiert werden kann, wurde ein eigenes Interaktionssystem entwickelt. Im Mittelpunkt stehen hierbei die Klasse ControllerScriptNew und das Interface BasicObject.
Im ControllerScriptNew werden die grundlegenden Funktionen der Controller bereitgestellt. Die Controller stellen vier relevante Inputs in Form von Buttons und Touchpad zur Verfügung:
Der Trigger, das Touchpad, der Menü Button und der Grip Button.

![6.png](https://bitbucket.org/repo/aEzoBe/images/3411133869-6.png)
 
Die Grundfunktionen dieser Tasten sind folgende:

* Trigger: Spawnen eines Objekts aus dem SpawnObjektPool.
* Grip: Greifen nach einem Objekt bzw. Binden eines Objekts an den Controller (attachedObjekt).
* Menu: Das Controllermenü wird aufgerufen.
* Touchpad: Bewegung durch Teleportation.

### AttachedObject ###
Damit ein Controller ein Objekt erkennt, ist dieser mit einem Collider versehen, welcher als Trigger fungiert. Wenn der Collider mit einem Objekt, welches das Interface BasicObject implementiert hat, in Berührung kommt, kann durch Druck des Grip Buttons das Objekt an den Controller gebunden werden. Die Referenz auf dieses Objekt wird in die Variable attachedObject geschrieben. Dadurch werden die Grundfunktionen durch die im attachedObject definierten Funktionen ersetzt.

### BasicObject Interface ###
Alle Objekte mit denen die Controller interagieren sollen, müssen das Interface BasicObject implementieren. 

Beispielfunktion


```
#!C#

public void triggerButton(int config, float triggerAxis, ControllerScriptNew source)
int config
```


 Da es mehrere Input Events für die einzelnen Buttons gibt (ButtonPressed, ButtonPressedUP, ButtonPressedDown) wird in config die Konstellation gespeichert. Dabei hat ButtonPressedDown den 1, ButtonPressed den 2 und ButtonPressedUP den 4. Durch die Addition dieser Werte lässt sich die der Parameter config ermitteln.

```
#!C#

float triggerAxis
```
Gibt die Stärke an mit der der Trigger gedrückt wird. Der Wert liegt zwischen 0 und 1.


```
#!C#

ControllerScriptNew source
```
Referenz auf den Controller.



Die anderen Funktionen sind nach demselben Prinzip aufgebaut.