﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public interface BasicObject {

    void triggerButton(int config, float triggerAxis, ControllerScriptNew source);
    void gripButton(int config, ControllerScriptNew source);
    void menuButton(int config, ControllerScriptNew source);

    void touchpadButton(int config, Vector2 touchpadAxis, ControllerScriptNew source);

    void initObject(ControllerScriptNew source);

    void drop();

    Sprite getThumbnail();
    

}
