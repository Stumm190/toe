﻿using UnityEngine;
using System.Collections;

public class CameraTracker : MonoBehaviour {

    public GameObject controllerL;
    public GameObject controllerR;
    public GameObject direction;
    [SerializeField]
    private float winkelL;
    [SerializeField]
    private float winkelR;
    [SerializeField]
    private Vector3 vektorL;
    [SerializeField]
    private Vector3 vektorR;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        vektorL = controllerL.transform.position - transform.position;
        vektorL.y = 0;
        Vector3 directionV = direction.transform.position - transform.position;
        directionV.y = 0;
        winkelL = Vector3.Angle(vektorL, directionV);
        winkelL *= Vector3.Cross(vektorL, directionV).y < 0 ? 1 : -1;
        vektorR = controllerR.transform.position - transform.position;
        vektorR.y = 0;
        winkelR = Vector3.Angle(vektorR, directionV);
        winkelR *= Vector3.Cross(vektorR, directionV).y < 0 ? 1 : -1;
	}

    void OnDrawGizmo()
    {
        Gizmos.color = new Color(0, 0, 160);
        Gizmos.DrawLine(transform.position, controllerL.transform.position);
        Gizmos.color = new Color(160, 0, 0);
        Gizmos.DrawLine(transform.position, controllerR.transform.position);
    }
}
