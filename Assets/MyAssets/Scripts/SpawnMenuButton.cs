﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

 

public class SpawnMenuButton : MonoBehaviour {

    public GameObject poolObject;
    public GameObject buttonPF;
    private ControllerScriptNew controllerScript;
    private GameObject[] pool;
    private MovingScript movingScript;
   
	// Use this for initialization
	void Awake () {
        movingScript = GameObject.Find("ItemFocus").GetComponent<MovingScript>();
	}

    public void setController()
    {
        controllerScript = transform.GetComponentInParent<ControllerScriptNew>();
        pool = poolObject.GetComponent<SpawnObjectPool>().objectPool;
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        foreach (GameObject g in pool)
        {
            if (g == controllerScript.spawnObject)
            {
                
                movingScript.setNewPosition(buttonSetup(g));
            }
            else
            {
                buttonSetup(g);
            }
            // button = (GameObject)GameObject.Instantiate(buttonPF, this.gameObject.transform.position, this.gameObject.transform.rotation);
            // button.transform.SetParent(this.gameObject.transform);
            
        }
    }

    GameObject buttonSetup(GameObject spawnableObject)
    {
        GameObject button;

        button = (GameObject)GameObject.Instantiate(buttonPF, this.gameObject.transform.position, this.gameObject.transform.rotation);

        UnityEngine.Events.UnityAction action1 = () => { controllerScript.chooseSpawnObject(spawnableObject); };
        button.GetComponent<Button>().onClick.AddListener(action1);

        UnityEngine.Events.UnityAction action2 = () => { movingScript.setNewPosition(button); };
        button.GetComponent<Button>().onClick.AddListener(action2);
        
           Sprite thumbnail;
          
           try
           {
               thumbnail = spawnableObject.GetComponent<BasicObject>().getThumbnail();
           }
           catch (NullReferenceException e)
           {
               thumbnail = null;
           }
        if (thumbnail != null)
        {
       // button.GetComponent<Button>().image.sprite = Sprite.Create(thumbnail, new Rect(0, 0, thumbnail.width, thumbnail.height), new Vector2(0.5f, 0.5f));
            button.GetComponent<Button>().image.sprite = thumbnail;
            

        }
        button.transform.SetParent(this.gameObject.transform);
        button.transform.localScale = new Vector3(1,1,1);
        return button;
    }
}
