﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class LevelMenuButton : MonoBehaviour {

    public GameObject buttonPF;
    private ControllerScriptNew controllerScript;
    public string[] levelPool;


    // Use this for initialization
    void Start()
    {
        //levelPool = new string[3];
        controllerScript = transform.GetComponentInParent<ControllerScriptNew>();
        Debug.Log(controllerScript == null);

        foreach (string s in levelPool)
        {
            // button = (GameObject)GameObject.Instantiate(buttonPF, this.gameObject.transform.position, this.gameObject.transform.rotation);
            // button.transform.SetParent(this.gameObject.transform);
            buttonSetup(s);
        }
    }


    void buttonSetup(string levelname)
    {
        GameObject button;

        button = (GameObject)GameObject.Instantiate(buttonPF, this.gameObject.transform.position, this.gameObject.transform.rotation);
        UnityEngine.Events.UnityAction action1 = () => { WorldFunctions.LoadScene(levelname); };
        button.GetComponent<Button>().onClick.AddListener(action1);
        button.GetComponentInChildren<Text>().text = levelname;
        button.transform.SetParent(this.gameObject.transform);
        button.transform.localScale = new Vector3(1, 1, 1);

    }
}
